﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeTest
{
    class Area
    {
        //public int reflectCeiling; //Коэфф отраж. потолка
        //public int reflectFloor; // Пола
        //public int reflectWalls // Стен
        public float x, y, h;
        private float lum = 0;
        private Mode lightingMode = Mode.economy;
        private Sensor sensor;
        public int targetLum
        {
            get;
            set;
        }
        public List<LightSource> lampList = new List<LightSource>(); //ловим сюда копию листа источников с комнаты и сортируем здесь.
        public List<float> indexRA = new List<float>(); //Индекс помещения/поверхности. Нужно связать для каждой лампы.
        public List<int> listIndex = new List<int>(); //Номер лампы

        public Area(float _x, float _y, float _h)
        {

        }

        public int GetSensorLum()
        {
            return sensor.Lum;
        }

        public int GetLum()
        {
            return Convert.ToInt32(lum); //программа должна считать в этот момент всю освещенность!
        }

        public int TestLum(int numLamp)
        {
            float testLum = 0;
            for (int j = 0; j < lampList.Count; j++)
            {
                testLum += lampList[j].power * indexRA[j];
            }
            if (lampList[numLamp].state != 100) testLum += (lampList[numLamp].maxPower - lampList[numLamp].power) * indexRA[numLamp];
            return Convert.ToInt32(testLum);
        }

        /*public void SetLum(int targetLum)
        {
            switch (lightingMode)
            {
                case Mode.economy:
                    SetLumEconomy(targetLum);
                    break;
                case Mode.economyB:
                    SetLumEconomyB(targetLum);
                    break;
                case Mode.blueFilter:
                    SetLumBlueFilter(targetLum);
                    break;
                case Mode.accurate:
                    SetLumAccurate(targetLum);
                    break;
                case Mode.absolute:
                    SetLumAbsolute(targetLum);
                    break;
                default:
                    //ошибка
                    break;
            }
        }*/

        /*private void SetLumEconomy(int _targetLum)
        {
            float testLum = 0;
            testLum = sensor.Lum; //окно, либо другие работающие источники (освещающие другие поверхности). 
            //Пока он попытается включить уже включенное
            //Можно попробовать отнять все включенные источники и узнать окно.
            int i = 0;
            for (i=0;  i<lampList.Count; i++)
            {
                testLum += lampList[i].maxPower * indexRA[i]; //добавить проверку, не включен ли ещё
                if (testLum < _targetLum) break;
            }
            int k = 0;
            while (k < i)
            {
                lampList[i].SetState(100);
                k++;
            }
            //Последняя из ламп не на полную мощность!
            float deltaLum = testLum - lampList[i].maxPower * indexRA[i];
            float needPower = deltaLum / indexRA[i];
            lampList[i].SetState(Convert.ToByte(needPower / lampList[i].maxPower)); //включаем на нужную мощность
        }*/

   /*     private void SetLumEconomyB(int _targetLum)
        {

        }
        private void SetLumBlueFilter(int _targetLum)
        {

        }
        private void SetLumAccurate(int _targetLum)
        {

        }
        private void SetLumAbsolute(int _targetLum)
        {

        }*/ //похоже, что не понадобится. Расчёт будет в Room

        public void AddSensor()
        {

        }

        public void RemoveSensor()
        {

        }

        public void RefrashLapmList(List<LightSource> _lampList, List<float> _indexRA, int _reflectCeiling, int _reflectFloor, int _reflectWalls)
        {
            lampList = _lampList;
            indexRA = _indexRA;

            if (_reflectCeiling == 70)
            {
                if (_reflectFloor == 50)
                {
                    if (_reflectWalls == 30) EfficiencyCalcType1();
                    else EfficiencyCalcType2();
                }
                else
                {
                    if (_reflectWalls == 30) EfficiencyCalcType3();
                    else EfficiencyCalcType4();
                }
            }
            else if (_reflectCeiling == 50)
            {
                if (_reflectFloor == 50)
                {
                    EfficiencyCalcType5();
                }
                else
                {
                    EfficiencyCalcType6();
                }
            }
            else EfficiencyCalcType7();
            EfficiencyCalc();
            //замена индекса освещенностью на Ватт
            bool sort = false;
            while (!sort)//сортировка
            {
                sort = true;
                for (int i = 1; i < indexRA.Count; i++)
                {
                    if (indexRA[i] > indexRA[i - 1]) //если КПД не по порядку в листе, то меняем местами два элемента с несоответствием 
                    {
                        float ra = indexRA[i];
                        indexRA[i] = indexRA[i - 1];
                        indexRA[i - 1] = ra;
                        LightSource l = lampList[i];
                        lampList[i] = lampList[i - 1];
                        lampList[i - 1] = l;
                        int la = listIndex[i];
                        listIndex[i] = listIndex[i - 1];
                        listIndex[i - 1] = la;
                        sort = false;
                    }
                }
            }
        }

        public void EfficiencyCalcType1() //70 50 30
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(5.04412 + 48.8007 * indexRoom - 12.8285 * stepen2 + 1.17355 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-2.95835 + 51.152 * indexRoom - 13.3432 * stepen2 + 1.2305 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(0.00832114 + 0.690463 * indexRoom - 0.198049 * stepen2 + 0.01953 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0449809 + 0.755701 * indexRoom - 0.218144 * stepen2 + 0.0214431 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.318263 + 0.670413 * indexRoom - 0.196832 * stepen2 + 0.0188954 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType2() //70 50 10
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(5.81557 + 45.7889 * indexRoom - 13.1214 * stepen2 + 1.27456 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-2.61931 + 48.7697 * indexRoom - 13.5744 * stepen2 + 1.29739 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(0.0459949 + 0.593594 * indexRoom - 0.169379 * stepen2 + 0.0164088 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0709689 + 0.672827 * indexRoom - 0.198337 * stepen2 + 0.0193887 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.365888 + 0.528139 * indexRoom - 0.156185 * stepen2 + 0.0149573 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType3() //70 30 30
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(-0.427639 + 47.5726 * indexRoom - 12.2058 * stepen2 + 1.11796 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-6.04677 + 48.6965 * indexRoom - 12.4833 * stepen2 + 1.14937 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(-0.022681 + 0.574842 * indexRoom - 0.151554 * stepen2 + 0.0143823 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.00503335 + 0.68665 * indexRoom - 0.187438 * stepen2 + 0.0178221 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.312853 + 0.592569 * indexRoom - 0.163518 * stepen2 + 0.0151496 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType4() //70 30 10
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(-0.614366 + 46.067 * indexRoom - 12.5449 * stepen2 + 1.18349 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-5.51853 + 45.4685 * indexRoom - 11.8606 * stepen2 + 1.09378 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(-0.0823303 + 0.698226 * indexRoom - 0.214884 * stepen2 + 0.0218677 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0485292 + 0.584444 * indexRoom - 0.155838 * stepen2 + 0.0144423 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.316088 + 0.544434 * indexRoom - 0.156805 * stepen2 + 0.014747 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType5() //50 50 10
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(3.17437 + 32.5957 * indexRoom - 9.23461 * stepen2 + 0.885869 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-2.01483 + 30.2748 * indexRoom - 7.5885 * stepen2 + 0.675021 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(0.0159949 + 0.593594 * indexRoom + -0.169379 * stepen2 + 0.0164088 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0697798 + 0.646338 * indexRoom - 0.188188 * stepen2 + 0.0182674 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.385816 + 0.477972 * indexRoom - 0.137106 * stepen2 + 0.0129084 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType6() //50 30 10
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(-1.1156 + 33.576 * indexRoom - 9.30232 * stepen2 + 0.878633 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-4.82316 + 32.2444 * indexRoom - 8.71986 * stepen2 + 0.828921 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(-0.0366833 + 0.571264 * indexRoom - 0.162318 * stepen2 + 0.0159106 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0180324 + 0.624928 * indexRoom - 0.17485 * stepen2 + 0.0166304 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.323995 + 0.529169 * indexRoom - 0.152714 * stepen2 + 0.0143969 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalcType7() //30 10 10
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                ClassLamp CL = lampList[i].lampClass; //ceiling, suspension, even, cosine, pervasive
                float indexRoom = indexRA[i];
                float stepen2 = Convert.ToSingle(Math.Pow(indexRoom, 2));
                float stepen3 = Convert.ToSingle(Math.Pow(indexRoom, 3));
                switch (CL)
                {
                    case ClassLamp.ceiling:
                        indexRA[i] = Convert.ToSingle(-2.32439 + 19.7534 * indexRoom - 5.47723 * stepen2 + 0.524062 * stepen3);
                        break;
                    case ClassLamp.suspension:
                        indexRA[i] = Convert.ToSingle(-2.21867 + 13.7494 * indexRoom - 2.73391 * stepen2 + 0.206554 * stepen3);
                        break;
                    case ClassLamp.even:
                        indexRA[i] = Convert.ToSingle(-0.050995 + 0.493877 * indexRoom - 0.133413 * stepen2 + 0.0128583 * stepen3);
                        break;
                    case ClassLamp.cosine:
                        indexRA[i] = Convert.ToSingle(0.0234625 + 0.521971 * indexRoom - 0.12952 * stepen2 + 0.0113181 * stepen3);
                        break;
                    case ClassLamp.pervasive:
                        indexRA[i] = Convert.ToSingle(0.277987 + 0.549838 * indexRoom - 0.160142 * stepen2 + 0.0151851 * stepen3);
                        break;
                    default:
                        break; //Jopka
                }
            }
        }

        public void EfficiencyCalc() //Светоотдача на Ватт
        {
            for (int i = 0; i < indexRA.Count; i++)
            {
                TypeLamp TL = lampList[i].lampType;
                //incandescent, fluorescent, galoger,  led //Накаливания, люминисцентная, галогенная, LED
                switch (TL)
                {
                    case TypeLamp.incandescent:
                        indexRA[i] *= 12;
                        break;
                    case TypeLamp.fluorescent:
                        indexRA[i] *= 65;
                        break;
                    case TypeLamp.galoger:
                        indexRA[i] *= 20;
                        break;
                    case TypeLamp.led:
                        indexRA[i] *= 100;
                        break;
                    default:
                        Console.WriteLine("unknown.");
                        break;
                }
            }
        }
    }
}

//public void ВКЛЮЧЕНИЕ ЛАМПЫ (НОМЕР ЛАМПЫ) !!!!!!!!!!!!!
