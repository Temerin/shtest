﻿enum ClassLamp : byte { ceiling = 1, suspension, even, cosine, pervasive } //Потолочный, подвесной, равномерный, косинусный, глубокий
enum Mode : byte { economy = 1, economyB,  blueFilter, accurate, absolute } //Экономичный, эконом+ (приоритет на регулировку), фильтр синего, точный, полный спектр
