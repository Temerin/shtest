﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeTest
{
    class LightSource         //Условно готовченко
    {
        public int colorfulTemperature;
        public int maxBrightness;
        public int maxPower;
        public TypeLamp lampType;
        public ClassLamp lampClass;
        public float x, y, h;
        public bool powerAdjustment; //Возможность регулировки по мощности

        public byte state; //текущее состояние, не bool, потому что регулировка
        public int power //текущая мощность
        {
            get;
        }
        public int luminousIntensity; //текущее излучение

        public LightSource(int _colorfulTemperature, int _maxBrightness, int _maxPower, TypeLamp _lampType,
            ClassLamp _lampClass, float _x, float _y, float _h, bool _powerAdjustment)
        {
            colorfulTemperature = _colorfulTemperature;
            maxBrightness = _maxBrightness;
            maxPower = _maxPower;
            lampType = _lampType;
            lampClass = _lampClass;
            x = _x;
            y = _y;
            h = _h;
            powerAdjustment = _powerAdjustment;
            state = 0;
            power = 0;
            luminousIntensity = 0;
        }

        public int SetState(byte _state)
        {
            if (powerAdjustment)
            {
                state = _state;
                return state;
            }
            else if (Convert.ToBoolean(_state)) state = 100;
            else state = 0;
            return state;
        }

    }
}
