﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeTest
{
    class Room
    {
        public int reflectCeiling; //Коэфф отраж. потолка
        public int reflectFloor; // Пола
        public int reflectWalls; // Стен
        public float length; //Длина
        public float width; //Ширина
        public float height; //Высота комнаты
        private Mode lightingMode = Mode.economy; //Режим освещения
        public List<float> effic = new List<float>(); //Лист КПД ламп (какие там будут КПД - по ситуации)
        public List<LightSource> lampList = new List<LightSource>();
        public List<Area> areaList = new List<Area>();
        public List<int> areaTargetLumList = new List<int>();
        public List<int> windowLum = new List<int>(); //Освещенность солнцем
        public List<int> listIndex = new List<int>(); //Номер лампы 

        public List<Area> setArea = new List<Area>(); //Поверхности, на которых устанавливается освещение
        public List<Byte> targetLamp = new List<Byte>(); //Лист с данными о будущем включении ламп

        public Room(int _reflectCeiling, int _reflectFloor, int _reflectWalls, float _length, float _width, float _height)
        {
            reflectCeiling = _reflectCeiling;
            reflectFloor = _reflectFloor;
            reflectWalls = _reflectWalls;
            length = _length;
            width = _width;
            height = _height;
        }

        public void SetMode(Mode _lightingMode)
        {
            lightingMode = _lightingMode;
            foreach (Area area in areaList)
            {
               // area.SetMode(lightingMode);
            }
        }

        public void AddLamp(int _colorfulTemperature, int _maxBrightness, int _maxPower, TypeLamp _lampType,
            ClassLamp _lampClass, float _x, float _y, float _h, bool _powerAdjustment)
        {
            lampList.Add(new LightSource(_colorfulTemperature, _maxBrightness, _maxPower, _lampType,
            _lampClass, _x, _y, _h, _powerAdjustment));

            
            foreach (Area area in areaList)
            {
                List<float> indexRA = new List<float>(); //индекс помещения для каждой лампы поверхности
                foreach (LightSource lamp in lampList)
                {
                    float s = length * width;
                    double dx, dy, dh;
                    double distance;
                    dx = area.x - lamp.x;
                    dy = area.y - lamp.y;
                    dh = area.h - lamp.h;
                    dx *= dx;
                    dy *= dy;
                    dh *= dh;
                    distance = dx + dy + dh;
                    distance = Math.Sqrt(distance);
                    distance *= length + width;
                    indexRA.Add(Convert.ToSingle(distance));
                }
                area.RefrashLapmList(lampList, indexRA, reflectCeiling, reflectFloor, reflectWalls);
            }

        }

        public void AddArea(float _x, float _y, float _h)
        {
            areaList.Add(new Area(_x, _y, _h));
            List<float> indexRA = new List<float>();
            foreach (LightSource lamp in lampList)
            {
                float s = length * width;
                double dx, dy, dh;
                double distance;
                dx = _x - lamp.x;
                dy = _y - lamp.y;
                dh = _h - lamp.h;
                dx *= dx;
                dy *= dy;
                dh *= dh;
                distance = dx + dy + dh;
                distance = Math.Sqrt(distance);
                distance *= length + width;
                indexRA.Add(Convert.ToSingle(distance));
            }
            areaList[areaList.Count].RefrashLapmList(lampList, indexRA, reflectCeiling, reflectFloor, reflectWalls);
        }
        public void SetTargetLum(int areaNum, int setlum)
        {
            areaList[areaNum].targetLum = setlum;
        }

        public void SetLumSeveralArea(Boolean first = true) //метод установки освещенности для нескольких поверхностей
        {
            if (first)
            {
                setArea = areaList.GetRange(0, areaList.Count);
            }
            //составляем список поверхностей
            AverageEfficiency(setArea); //средние КПД
            LampListSort(); //сортируем по среднему КПД
            if (first) //если это самая первая поверхность, и мы ещё ничего не включали
            {
                for (int i = 0; i < areaList.Count; i++)
                {
                    windowLum[i] = areaList[i].GetSensorLum();
                }
            }
            if (setArea[0].GetLum() < setArea[0].targetLum)
            {
                SetLumArea(setArea[0], setArea[0].targetLum, windowLum[areaList.BinarySearch(setArea[0])]); // (находит поверхность в общем списке, и освещенность окном для неё)
            }
        }

        public void AverageEfficiency(List<Area> setArea) //лист средних КПД
        { 
            for (int i = 0; i < effic.Count; i++)
            {
                effic[i] = 0;
            }
            foreach (Area area in setArea)
            {
                for (int i = 0; i < area.listIndex.Count; i++)
                {
                    effic[area.listIndex[i]] += area.indexRA[i] / setArea.Count; // суммирует Лм / Ватт из каждого списка и делит на кол - во поверхностей
                }
            }
        }
        public void LampListSort()
        {
            bool sort = false;
            while (!sort)//сортировка
            {
                sort = true;
                for (int i = 1; i < effic.Count; i++)
                {
                    if (effic[i] > effic[i - 1]) //если КПД не по порядку в листе, то меняем местами два элемента с несоответствием 
                    {
                        float ra = effic[i];
                        effic[i] = effic[i - 1];
                        effic[i - 1] = ra;
                        LightSource l = lampList[i];
                        lampList[i] = lampList[i - 1];
                        lampList[i - 1] = l;
                        int la = listIndex[i];
                        listIndex[i] = listIndex[i - 1];
                        listIndex[i - 1] = la;
                        sort = false;
                    }
                }
            }
        } 
        public int SetLumArea(Area area, int setlum, int window)
        {
            int lum = 0;
            int areaNum = areaList.BinarySearch(area);
            int i = 0;
            while ((area.TestLum(i) + window) < setlum)
            {

            }
            //проверка на включенность лампы
            return lum;
        }

        public int SetLumEconomy(int areaNum, int setlum)
        {
            int lum = 0;
            float testLum = 0;
            testLum = areaList[areaNum].GetSensorLum(); //окно, либо другие работающие источники (освещающие другие поверхности). 
            //Пока он попытается включить уже включенное
            //Можно попробовать отнять все включенные источники и узнать окно.
            int i = 0;
            for (i = 0; i < areaList[areaNum].lampList.Count; i++)
            {
                testLum += areaList[areaNum].lampList[i].maxPower * areaList[areaNum].indexRA[i]; //добавить проверку, не включен ли ещё
                if (testLum < setlum) break;
            }
            int k = 0;
            while (k < i)
            {
                areaList[areaNum].lampList[i].SetState(100);
                k++;
            }
            //Последняя из ламп не на полную мощность!
            float deltaLum = testLum - areaList[areaNum].lampList[i].maxPower * areaList[areaNum].indexRA[i];
            float needPower = deltaLum / areaList[areaNum].indexRA[i];
            areaList[areaNum].lampList[i].SetState(Convert.ToByte(needPower / areaList[areaNum].lampList[i].maxPower)); //включаем на нужную мощность


            return lum;
        }

        
        //метод для установки освещенности поверхности
        public int SetAreaLum(int areaNum, int setLum) //!!!!!!!!
        {
            int lum = 0;
            lum = SetLumEconomy(areaNum, setLum);
            areaList[areaNum].GetLum();
            return lum;
        }

        public void RemoveLamp()
        {
            //Переделать список и...
            List<float> indexRA = new List<float>();
            foreach (Area area in areaList)
            {
                area.RefrashLapmList(lampList, indexRA, reflectCeiling, reflectFloor, reflectWalls);
            }
        }

        public void RemoveArea()
        { }

        public void AllOn()
        { }

        public void AllOff()
        { }
    }
}
