﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartHomeTest
{
    class Sensor
    {
        private int lum;
        public Sensor()
        {
            lum = 0;
        }

        public int Lum => lum;
    }
}
